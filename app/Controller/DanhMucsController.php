<?php
/**
 * Created by PhpStorm.
 * User: Quang
 * Date: 2017-04-11
 * Time: 10:36 PM
 */
class DanhMucsController extends AppController
{
    public $name = "DanhMucs";
    public $uses = array('Danhmuc');

    public function beforeFilter() {
        parent::beforeFilter();
        if(!$this->Session->check('username')){
            return $this->redirect(['controller' => 'users', 'action' => 'index', 'login']);
        }
    }

    public function index()
    {
        //$data = $this->Danhmuc->find('all');
        $data = $this->paginate('Danhmuc', array(), array('menhgia'));
        $this->set('data', $data);
    }

    public function edit(){
        $id = $this->request->params['pass'][0];
        $data = $this->Danhmuc->read(null,$id);
        $this->set('data',$data);
    }

    public function update(){
        $id = $this->request->query['id'];
        $tendanhmuc = $this->request->query['tendanhmuc'];
        $loaisach = $this->request->query['loaisach'];
        $tacgia = $this->request->query['tacgia'];
        $menhgia = $this->request->query['menhgia'];
        $this->Danhmuc->set(array('id' => $id, 'danhmuc_name' => $tendanhmuc, 'loaisach' => $loaisach, 'tacgia' => $tacgia,
            'menhgia' => $menhgia));
        $this->Danhmuc->save();
        return $this->redirect(['controller' => 'danhmucs', 'action' => 'index']);
    }

    public function delete(){
        $id = $this->request->params['pass'][0];
        if($this->Danhmuc->deleteAll(array('Danhmuc.id' => $id), false) == true){
            return $this->redirect(['controller' => 'danhmucs', 'action' => 'index']);
        } else{
            echo 'Không có dữ liệu để xóa';
        }
    }

    public function insert(){
        $data = $this->request->query;
        if(!empty($data['loaisach'])){
            $this->Danhmuc->save($data);
            return $this->redirect(['controller' => 'danhmucs', 'action' => 'index']);
        }else{
            echo "Lưu không thành công";
        }
    }
}