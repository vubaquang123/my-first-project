    <section class="content">
         <div class="row">
           <div class="col-md-12">
             <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Insert dữ liệu</h3>
                 <div class="box-tools pull-right">
                   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                   </button>
                   <div class="btn-group">
                     <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                       <i class="fa fa-wrench"></i></button>
                     <!-- setting -->
                     <ul class="dropdown-menu" role="menu">
                       <li><a href="#">Action</a></li>
                       <li><a href="#">Another action</a></li>
                       <li><a href="#">Something else here</a></li>
                       <li class="divider"></li>
                       <li><a href="#">Separated link</a></li>
                     </ul>
                     <!-- -->
                   </div>
                   <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                 </div>
               </div>
               <!-- /.box-header -->
               <div class="box-footer">
                 <?php echo $this->Form->create(false, array('url' => array('controller' => 'danhmucs','action' => 'insert'), 'type' => 'get')); ?>
                    <div id="content">
                        <div class="form-group has-feedback">
                            <?php echo $this->Form->input('Danhmuc.danhmuc_name', array(
                                'label' => 'Tên sách', 'class' => 'form-control'
                                )); ?>
                        </div>
                        <div class="form-group has-feedback">
                            <?php echo $this->Form->input('Danhmuc.loaisach', array(
                                'label' => 'Loại sách :', 'class' => 'form-control'
                                )); ?>
                        </div>
                        <div class="form-group has-feedback">
                            <?php echo $this->Form->input('Danhmuc.tacgia', array(
                                'label' => 'Tác giả :', 'class' => 'form-control'
                                )); ?>
                        </div>
                        <div class="form-group has-feedback">
                            <?php echo $this->Form->input('Danhmuc.menhgia', array(
                                'label' => 'Mệnh giá :', 'class' => 'form-control'
                            )); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Html->tag('input',null,array('name' => 'login','type'=>'submit', 'class' => 'btn btn-primary',
                             'value' => 'Save')); ?>
                        </div>
                    </div>
                 <?php echo $this->Form->end(); ?>
               </div>
               <!-- /.box-footer -->
             </div>
             <!-- /.box -->
           </div>
           <!-- /.col -->
         </div>
         <div class="form-group">
             <table class="table">
             <tr><th>ID</th>
             <th>Tên danh mục</th>
             <th>Loại sách</th>
             <th>Tác giả</th>
             <th>Mệnh giá</th>
             <th colspan=2>Thao tác</th></tr>
              <?php
                 foreach($data as $item){
                  ?>
                   <tr>
                      <td>
                          <?php echo $item['Danhmuc']['id']; ?>
                             </td>
                             <td><?php echo $item['Danhmuc']['danhmuc_name']; ?></td>
                             <td><?php echo $item['Danhmuc']['loaisach']; ?></td>
                             <td><?php echo $item['Danhmuc']['tacgia']; ?></td>
                             <td><?php echo $item['Danhmuc']['menhgia']; ?></td>
                             <td><a href="danhmucs/edit/<?php echo $item['Danhmuc']['id']; ?>">Edit</a></td>
                             <td><a href="danhmucs/delete/<?php echo $item['Danhmuc']['id']; ?>"
                                onclick="confirm_delete()" >Delete</a></td>
                      </tr>
                <?php
                  }
                ?>
            </table>
         </div>
 </section>
<script>
    function confirm_delete(){
        confirm("Do you want to delete this recode");
    }
</script>
