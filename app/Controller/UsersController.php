<?php
App::uses('AppController', 'Controller');
/**
 * Created by PhpStorm.
 * User: Quang
 * Date: 2017-04-14
 * Time: 11:59 PM
 */
class UsersController extends AppController
{
    public $name = "Users";
    public $helpers = array("Html");
    public $component = array("Session");
    public $uses = array('User');
    public $layout = "login";
    public function index(){}

    public function login(){
        $account = $this->request->data('username');
        $password = $this->request->data('password');
        if($this->User->checkLogin($account,$password) == true){
            $this->Session->write('username', $account);
            $this->redirect(['controller' => 'Danhmucs', 'action' => 'index']);
        }else{
            $error = $this->Session->setFlash("Thông tin đăng nhập không chính xác", 'default', array(), 'error');
            $this->set('error',$error);
        }
    }

    public function logout(){
        $this->Session->delete('username');
        $this->redirect('login');
    }
}