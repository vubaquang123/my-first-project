<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <?php echo $this->Html->css("bootstrap.min") ?>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <?php echo $this->Html->css("https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css") ?>
  <!-- Ionicons -->
  <?php echo $this->Html->css("https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css") ?>
  <!-- Theme style -->
  <?php echo $this->Html->css("AdminLTE.min") ?>
  <!-- iCheck -->
  <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">
  <?php echo $this->Html->css("blue") ?>
  <?php echo $this->Html->script("https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js") ?>
  <?php echo $this->Html->script("https://oss.maxcdn.com/respond/1.4.2/respond.min.js") ?>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Admin</b>LTE</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <?php echo $this->Form->create(false, array('url' => array('controller' => 'users','action' => 'login'), 'type' => 'post')); ?>
      <div class="form-group has-feedback">
        <?php echo $this->Html->tag('input',null, array('type' => 'text', 'id' => 'username', 'class' => 'form-control','name' => 'username',
    'placeholder' => "Username :")) ?>
      </div>
      <div class="form-group has-feedback">
        <?php echo $this->Html->tag('input',null, array('type' => 'password', 'id' => 'password', 'class' => 'form-control', 'name' => 'password',
		'placeholder' => 'Password :')) ?>
      <span class="glyphicon glyphicon-envelope form-control-feedback"><?php echo $error ?></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
           <?php echo $this->Html->tag('input',null,array('name' => 'login','type'=>'submit', 'class' => 'btn btn-primary btn-block btn-flat', 'value' => 'Login')) ?>
        </div>
        <!-- /.col -->
      </div>
    <?php echo $this->Form->end() ?>
    <!-- /.social-auth-links -->

    <a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<?php echo $this->Html->script("jquery-2.2.3.min.js") ?>
<!-- Bootstrap 3.3.6 -->
<?php echo $this->Html->script("bootstrap.min.js") ?>
<!-- iCheck -->
<?php echo $this->Html->script("icheck.min.js") ?>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
