<section class="content">
         <div class="row">
           <div class="col-md-12">
             <div class="box">
               <div class="box-header with-border">
                 <h3 class="box-title">Update dữ liệu</h3>
                 <div class="box-tools pull-right">
                   <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                   </button>
                   <div class="btn-group">
                     <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                       <i class="fa fa-wrench"></i></button>
                     <!-- setting -->
                     <ul class="dropdown-menu" role="menu">
                       <li><a href="#">Action</a></li>
                       <li><a href="#">Another action</a></li>
                       <li><a href="#">Something else here</a></li>
                       <li class="divider"></li>
                       <li><a href="#">Separated link</a></li>
                     </ul>
                     <!-- -->
                   </div>
                   <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                 </div>
               </div>
               <!-- /.box-header -->
               <div class="box-footer">
                  <?php echo $this->Form->create(false, array('url' => array('controller' => 'danhmucs','action' => 'update'), 'type' => 'get')); ?>
                      <div id="content">
                         <div class="form-group">
                            <label class="col-sm-2" >Id sách :</label>
                            <div class="col-sm-10"><input type="text" value="<?php
                            echo ($data['Danhmuc']['id']); ?>" class="form-control" readonly="true" id="id" name="id" /></div>
                         </div>
                         <div class="form-group">
                            <label class="col-sm-2" >Tên sách :</label>
                            <div class="col-sm-10"><input type="text" value="<?php
                            echo ($data['Danhmuc']['danhmuc_name']); ?>" class="form-control" id="tendanhmuc" name="tendanhmuc" /></div>
                         </div>
                         <div class="form-group">
                             <label class="col-sm-2" >Loại sách :</label>
                             <div class="col-sm-10"><input type="text" value="<?php
                             echo ($data['Danhmuc']['loaisach']); ?>" class="form-control" id="loaisach" name="loaisach"/></div>
                         </div>
                         <div class="form-group">
                            <label class="col-sm-2" >Tác giả :</label>
                            <div class="col-sm-10"><input type="text" value="<?php
                            echo ($data['Danhmuc']['tacgia']); ?>" class="form-control" id="tacgia" name="tacgia" /></div>
                         </div>
                         <div class="form-group">
                             <label class="col-sm-2" >Mệnh giá :</label>
                             <div class="col-sm-10"><input type="text" value="<?php
                             echo ($data['Danhmuc']['menhgia']); ?>" class="form-control" id="menhgia" name="menhgia" /></div>
                         </div>
                         <div class="form-group">
                            <input type="submit" value="Update" />
                         </div>
                      </div>
                  <?php echo $this->Form->end() ?>
               </div>
               <!-- /.box-footer -->
             </div>
             <!-- /.box -->
           </div>
         <!-- /.col -->
       </div>
    </div>
 </section>

